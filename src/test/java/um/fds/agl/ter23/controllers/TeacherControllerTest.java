package um.fds.agl.ter23.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter23.entities.Teacher;
import um.fds.agl.ter23.services.TeacherService;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private TeacherService teacherService;

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        assumingThat(teacherService.getTeacher(10L).isEmpty(), () -> {
            mvc.perform(post("/addTeacher")
                            .param("firstName", "Anne-Marie")
                            .param("lastName", "Kermarrec")
                            .param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
        });
        Optional<Teacher> teacher = teacherService.getTeacher(10L);
        assertThat(teacher, is(notNullValue()));
        assertEquals(teacher.get().getLastName(), "Kermarrec");
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        assumingThat(teacherService.getTeacher(10L).isEmpty(), () -> {
            mvc.perform(post("/addTeacher")
                            .param("firstName", "Anne-Marie")
                            .param("lastName", "Kermarrec")
                            .param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
        });
        Optional<Teacher> teacher = teacherService.getTeacher(10L);
        assertThat(teacher, is(notNullValue()));
        assertEquals(teacher.get().getLastName(), "Kermarrec");
        assertEquals(teacher.get().getFirstName(), "Anne-Marie");
        mvc.perform(post("/addTeacher")
                        .param("firstName", "Samuel")
                        .param("lastName", "CASAS DRAY")
                        .param("id", "10")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        teacher = teacherService.getTeacher(10L);
        assertEquals(teacher.get().getLastName(), "CASAS DRAY");
        assertEquals(teacher.get().getFirstName(), "Samuel");
    }
}