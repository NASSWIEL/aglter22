package um.fds.agl.ter23.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter23.entities.Teacher;
import um.fds.agl.ter23.forms.TeacherForm;
import um.fds.agl.ter23.repositories.TeacherRepository;
import um.fds.agl.ter23.services.TeacherService;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TeacherControllerTest2 {
    @Autowired
    private MockMvc mvc;

    @MockBean
    TeacherService teacherService;

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() {
        Teacher teacherBase = new Teacher("Anne-Marie", "Kermarrec", null);
        teacherBase.setId(10L);
        when(teacherService.getTeacher(10L)).thenReturn(Optional.empty());
        when(teacherService.saveTeacher(any(Teacher.class))).thenReturn(new Teacher());
        assumingThat(teacherService.getTeacher(10L).isEmpty(), () -> {
            mvc.perform(post("/addTeacher")
                            .param("firstName", teacherBase.getFirstName())
                            .param("lastName", teacherBase.getLastName())
                            .param("id", String.valueOf(teacherBase.getId()))
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
        });
        when(teacherService.getTeacher(10L)).thenReturn(Optional.of(teacherBase));
        Optional<Teacher> teacher = teacherService.getTeacher(10L);
        assertThat(teacher, is(notNullValue()));
        assertEquals(teacher.get().getLastName(), teacherBase.getLastName());
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        Teacher teacherBase = new Teacher("Anne-Marie", "Kermarrec", null);
        Teacher teacherBase2 = new Teacher("Samuel", "CASAS DRAY", null);
        teacherBase.setId(10L);
        teacherBase2.setId(10L);
        when(teacherService.getTeacher(10L)).thenReturn(Optional.empty());
        when(teacherService.saveTeacher(any(Teacher.class))).thenReturn(new Teacher());
        assumingThat(teacherService.getTeacher(10L).isEmpty(), () -> {
            mvc.perform(post("/addTeacher")
                            .param("firstName", teacherBase.getFirstName())
                            .param("lastName", teacherBase.getLastName())
                            .param("id", String.valueOf(teacherBase.getId()))
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
        });
        when(teacherService.getTeacher(10L)).thenReturn(Optional.of(teacherBase));
        Optional<Teacher> teacher = teacherService.getTeacher(10L);
        assertThat(teacher, is(notNullValue()));
        assertEquals(teacher.get().getLastName(), teacherBase.getLastName());
        assertEquals(teacher.get().getFirstName(), teacherBase.getFirstName());
        when(teacherService.getTeacher(10L)).thenReturn(Optional.of(teacherBase2));
        mvc.perform(post("/addTeacher")
                        .param("firstName", teacherBase2.getFirstName())
                        .param("lastName", teacherBase2.getLastName())
                        .param("id", String.valueOf(teacherBase2.getId()))
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        teacher = teacherService.getTeacher(10L);
        assertEquals(teacher.get().getLastName(), teacherBase2.getLastName());
        assertEquals(teacher.get().getFirstName(), teacherBase2.getFirstName());
    }

}
