package um.fds.agl.ter23.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class StudentGroup {
    private @Id
    @GeneratedValue Long id;
    private String nom;

    public StudentGroup(String nom) {
        this.nom = nom;
    }

    public StudentGroup(Long id, String nom) {
        this(nom);
        this.id = id;
    }

    public StudentGroup() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
