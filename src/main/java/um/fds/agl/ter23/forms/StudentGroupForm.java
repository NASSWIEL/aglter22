package um.fds.agl.ter23.forms;

public class StudentGroupForm {
    private String nom;
    private long id;

    public StudentGroupForm(long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public StudentGroupForm() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
