package um.fds.agl.ter23.forms;

import um.fds.agl.ter23.entities.Teacher;

public class TERForm {
    private Long teacher;
    private Long teacherSec;
    private long id;
    private String subject;

    public TERForm(long id, Long teacher, String subject , Long teacherSec) {
        this.teacher = teacher;
        this.subject = subject;
        this.id = id;
        this.teacherSec = teacherSec;
    }

    public TERForm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getTeacher() {
        return teacher;
    }

    public void setTeacher(Long teacher) {
        this.teacher = teacher;
    }

    public Long getTeacherSec() {
        return teacherSec;
    }

    public void setTeacherSec(Long teacherSec) {
        this.teacherSec = teacherSec;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
