package um.fds.agl.ter23.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter23.entities.StudentGroup;
import um.fds.agl.ter23.forms.StudentGroupForm;
import um.fds.agl.ter23.services.StudentGroupService;

@Controller
public class StudentGroupController {

    @Autowired
    private StudentGroupService studentGroupService;

    @GetMapping("/listStudentGroups")
    public Iterable<StudentGroup> getStudentGroups(Model model) {
        model.addAttribute("studentgroups", studentGroupService.getStudentGroups());
        return studentGroupService.getStudentGroups();
    }
    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_STUDENT')")
    @GetMapping(value = { "/addStudentGroup" })
    public String showAddPersonPage(Model model) {

        StudentGroupForm StudentGroupForm = new StudentGroupForm();
        model.addAttribute("studentGroupForm", StudentGroupForm);

        return "addStudentGroup";
    }

    @PostMapping(value = { "/addStudentGroup"})
    public String addStudentGroup(Model model, @ModelAttribute("StudentGroupForm") StudentGroupForm studentGroupForm) {
        StudentGroup studentGroup;
        if(studentGroupService.findById(studentGroupForm.getId()).isPresent()){
            studentGroup = studentGroupService.findById(studentGroupForm.getId()).get();
            studentGroup.setNom(studentGroupForm.getNom());
        } else {
            studentGroup = new StudentGroup(studentGroupForm.getNom());
        }
        studentGroupService.saveStudentGroup(studentGroup);
        return "redirect:/listStudentGroups";

    }

    @GetMapping(value = {"/showStudentGroupUpdateForm/{id}"})
    public String showStudentGroupUpdateForm(Model model, @PathVariable(value = "id") long id){

        StudentGroupForm studentGroupForm = new StudentGroupForm(id, studentGroupService.findById(id).get().getNom());
        model.addAttribute("studentGroupForm", studentGroupForm);
        return "updateStudentGroup";
    }

    @GetMapping(value = {"/deleteStudentGroup/{id}"})
    public String deleteStudentGroup(Model model, @PathVariable(value = "id") long id){
        studentGroupService.deleteStudentGroup(id);
        return "redirect:/listStudentGroups";
    }
}
