package um.fds.agl.ter23.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter23.entities.StudentGroup;

public interface StudentGroupRepository extends CrudRepository<StudentGroup, Long> {

    @Override
    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_STUDENT')")
    StudentGroup save(@Param("studentgroup") StudentGroup ter);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("studentgroup") StudentGroup ter);
}
